import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EditComponent} from './edit.component';
import {Address, Person, SearchService} from "../shared";
import {ActivatedRoute} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {of} from "rxjs";

describe('EditComponent', () => {
    let mockSearchService: SearchService;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [EditComponent],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            params: {id: 1}
                        }
                    }
                }
            ],
            imports: [FormsModule, HttpClientTestingModule]
        }).compileComponents();

        mockSearchService = TestBed.inject(SearchService);
    });

    it('should fetch a signle record', () => {
        const fixture = TestBed.createComponent(EditComponent);

        const person = new Person({id: 1, name: 'Jason Statham'});
        person.address = new Address({city: 'Michigan'});

        // mock response
        spyOn(mockSearchService, 'get').and.returnValue(of(person));

        // initialize component
        fixture.detectChanges();

        // verify service was called
        expect(mockSearchService.get).toHaveBeenCalledWith(1);

        // verify data was set on component when initialized
        const editComponent = fixture.componentInstance;
        expect(editComponent.person.address.city).toBe('Michigan');

        // verify HTML renders as expected
        const compiled = fixture.nativeElement;
        expect(compiled.querySelector('h3').innerHTML).toBe('Jason Statham');
    });
});
