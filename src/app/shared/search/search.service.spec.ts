import {TestBed} from '@angular/core/testing';

import {Person, SearchService} from './search.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

describe('SearchService', () => {
    let service: SearchService;
    let httpMock: HttpTestingController;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [SearchService]
        });
        service = TestBed.inject(SearchService);
        httpMock = TestBed.inject(HttpTestingController);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should retrieve all search results', (done) => {
        const mockResponse = [
            {name: 'Chuck Norris'},
            {name: 'Bruce Lee'},
            {name: 'Dolph Lungren'}
        ];

        service.getAll().subscribe((people: any) => {
            expect(people.length).toBe(3);
            expect(people[0].name).toBe('Chuck Norris');
            expect(people).toEqual(mockResponse);
            done();
        });

        const req = httpMock.expectOne('assets/data/people.json');
        expect(req.request.method).toBe('GET');
        req.flush(mockResponse);
    });

    it('should filter by search term', (done) => {
        const mockResponse = [{name: 'Chuck Norris'}];

        service.search('chu').subscribe((people: any) => {
            expect(people.length).toBe(1);
            expect(people[0].name).toBe('Chuck Norris');
            done();
        });

        const req = httpMock.expectOne('assets/data/people.json');
        expect(req.request.method).toBe('GET');
        req.flush(mockResponse);
    });

    it('should fetch by id', (done) => {
        const mockResponse = [
            {id: 1, name: 'Chuck Norris'},
            {id: 2, name: 'Bruce Lee'},
            {id: 3, name: 'Dolph Lungren'}
        ];

        service.get(3).subscribe((person: any) => {
            expect(person.name).toBe('Dolph Lungren');
            done();
        });
        const req = httpMock.expectOne('assets/data/people.json');
        expect(req.request.method).toBe('GET');
        req.flush(mockResponse);
    });

    afterEach(() => {
        httpMock.verify();
    });
});
